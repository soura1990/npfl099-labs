# NPFL099-labs

This repository hosts lab instructions and assignments for the [Statistical Dialogue Systems course](http://ufal.mff.cuni.cz/courses/npfl099) in Winter Semester 2020.

* [Dataset Assignment](datasets.md)
* [Semester Projects](projects.md)