 PersonaChat

Site: http://convai.io/#personachat-convai2-dataset
Description on GitHub: https://github.com/aliannejadi/ClariQ
Paper: http://convai.io/ConvAI3_ClariQ2020.pdf

The dataset is collected for the **ClariQ** challenge, organized as part of the Conversational AI challenge series at [Search-oriented Conversational AI EMNLP workshop](https://scai.info/) in 2020. The goal of the challenge is to deal with situations when a user *makes ambiguos requests*. In particular, the main research questions are as follows:
- **RQ1:** When to ask clarifying questions during dialogues?
- **RQ2:** How to generate the clarifying questions?

There were two stages in the challenge, but I will focus only on the first one, because that’s when the model training took place and the dataset was involved, whereas the second stage focused on human evaluation of the systems.

So, the dataset was created for specific tasks, and its domain may be characterized as ‘open-domain information-seeking conversations’. The training data includes:

- **User Request:** an initial user request in the conversational form with a label that reflects if clarification is needed [1-4];
- **Clarification questions:** a set of possible clarifying questions;
- **User Answers:** each question is supplied with a user answer.

In the challenge, the models, given a user request, had to return 1) a score [1 −4] indicating the necessity of asking clarifying questions, and 2) the most suitable clarifying question from the provided question bank. The testing data included 1) a set of user requests in a conversational form and 2) a question bank that contains all the questions from the collection.

The **ClariQ** dataset extends the dataset [Qualac](https://github.com/aliannejadi/qulac). The training data is taken mostly from it, but some new topics, questions, and answers are added as well. The test set is completely unseen and newly collected.

There’s no information about how ClariQ itself was collected, but as it builds upon Qualac, we can assume that the procedure was the same. Qulac is built on top of the [TREC Web Track 09-12](https://plg.uwaterloo.ca/~trecweb/2012.html) collections. As such, Qulac was collected as follows:

1. topics and their corresponding facets were defined, borrowed from the TREC Web Track;
2. several candidates clarifying questions for each query were collected through crowdsourcing;
3. the relevance of the questions to each facet was assessed, and new questions were collected for those facets that required more specific questions;
4. the answers for every query-facet-question triplet were collected.

That is, crowd workers helped with both creating and labelling the dataset.

The ClariQ dataset is stored as TSV files. More precisely, train and dev sets’ files consist of topics (queries), facets (information needs), clarifying questions, user's answers, and labels for how much clarification is needed. Test set files include test topic ID's, as well as text queries.

There are some additional files, for example, question bank (TSV file containing all the questions in the collection, as well as their ID's) and some other files, but they are not part of the main dataset, so I am not covering them here.

Also, I could not find any information about the license for ClariQ, but Qualac is licensed under the MIT License.

_________________________________________________

Speaking of statistical features of the data, I will focus only on the train set. The first lines of the training set look as follows:

| topic_id | initial_request | topic_desc | clarification_need | facet_id | facet_desc | question_id | question | answer
| ------ | ------ | ----- | ------ | ------ | ----- | ----- | ----- | ----- |
| 1 | Tell me about Obama family tree | Find information on President Barack Obama\'s family history, including genealogy, national origins, places and dates of birth, etc. | 2 | F0001 | Find the TIME magazine photo essay "Barack Obama's Family Tree". | Q00384 | are you interested in seeing barack obamas family | yes am interested in obamas family
| 1 | Tell me about Obama family tree | Find information on President Barack Obama\'s family history, including genealogy, national origins, places and dates of birth, etc. | 2 | F0001 | Find the TIME magazine photo essay "Barack Obama's Family Tree". | Q03442 | would you like to know barack obamas geneology | yes i want to know who made up his family
| 1 | Tell me about Obama family tree | Find information on President Barack Obama\'s family history, including genealogy, national origins, places and dates of birth, etc. | 2 | F0001 | Find the TIME magazine photo essay "Barack Obama's Family Tree". | Q03442 | would you like to know about obamas ancestors | yes this is what am looking for

There are **9176** instances in the training set. The “dialogue” part of each instance includes the initial request, the clarifying question, and the user answer. This means that there are 9176 small dialogues in the training set. However, there are **610** cases where no specifying questions are asked. Apart from them, each dialogue includes **three turns**, and each turn consists of **one sentence**.

Analyzing the data, I fould out that there are **187** different topics (initial requests) and **638** different facets (underlying information needs). There are **2403** different questions and **7017** different answers. The distribution of the “clarification need” (CN) parameter is as follows: **676** requests with *CN=1*, **3546** requests where *CN=2*, **3474** with *CN=3*, and the rest **1480** requests have *CN=4*.

For the following statistical features, I have taken only the “dialogue” part of each instance in the training set: the initial request, the clarifying question, and the user answer, leaving aside descriptions of topics and facets. All parameters I compute on each category separately, as well as on all data together.

|  | Overall | Requests | Questions | Answers 
| ------ | ------ | ----- | ------ | ------ | 
| **Sentences** | 26,308 | 9,176 | 8,566 | 8,566
| **Words** | 211,373 | 58,863 | 80,893 | 71,617
| **Mean** | 7.68 | 6.41 | 8.82 | 7.8
| **STD** | 3.72 | 1.98 | 3.44 | 4.78
| **Vocabulary size** | 4,102 | 473 | 2,392 | 3,320
| **Entropy (log2)** | 8.2 | 7.0 | 7.6 | 7.94

Note that I do not count mean and standard deviation for dialogues, turns, and sentences, because almost all instances in the training set consist of exactly three turns, with exactly one sentence per turn.

_________________________________________________

Looking at the data closer, I could see that it looks natural, especially the requests and answers. Some specifying questions, however, are strange if we consider them questions from a search agent. For example, for a request “Tell me about of Ralph Owen Brester”, there are questions like “how old is ralph owen brewseter” or “who is ralph owen brewster”. They are not proper specifying questions, because they don’t help to find out the user’s information need. Unsurprisingly, the answers for them are not helpful either: e.g. “i am not sure so i want to know” or just “i dont know”.

Another difficulty is the spelling mistakes that occur both in questions and answers. If the user is allowed to make spelling mistakes, the system should not! Both this and the occurence of irrelevant questions described above make learning from this dataset harder.

Summing up, the biggest limitation of the data, in my opinion, is the fact that clarifying questions were generated by people via crowdsourcing, and some of them are just not what you would expect from a search system to ask you. However, the dataset can still benefit an actual (search) system a lot. Though the data is not perfect, it deals with a vital issue of ambiguous requests, and if incorporated in a search system, such a method of asking clarifying questions can boost the relevance of results.
