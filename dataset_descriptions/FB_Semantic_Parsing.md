This document briefs about the dataset - Semantic Parsing for Task Oriented Dialog Datasets created by Sonal et al. (2018). 
The metrics presented below describes the statistics of this dataset.
The dataset has utterances that are focused on navigation, events, and navigation to events.

To create the datasets, Sonal et al. (2018), have asked crowdsourced workers to generate natural language sentences that they would ask a system that could assist in navigation and event queries. 
These requests were then labeled by two annotators. If these annotations weren’t identical then they adjudicated with a third annotator. 
If all three annotators disagreed then they discarded the utterance and its annotations. 
63.40% of utterances were resolved with 2 annotations and 94.09% were resolved after getting 3 annotations. 
They also compared percentage of utterances that were resolved after 2 annotations for depth ≤ 2 (traditional slot filling) and for depth > 2 (compositional): 68.87% vs 62.03%, noting that the agreement rate is similar.

Format (each row):
------------------
The format for each row is described below,

raw_utterance <tab> tokenized_utterance <tab> TOP-representation

where the TOP-representation is an annotated version of the utterance

e.g. Art fairs this weekend in Detroit <tab> [IN:GET_EVENT [SL:CATEGORY_EVENT Art fairs ] [SL:DATE_TIME this weekend ] in [SL:LOCATION Detroit ] ]
Opening of a non-terminal node is marked by "[" (part of the non-terminal label), while a closing is marked by a standalone "]"

License:
--------
Provided under the CC-BY-SA license.

Details of the datasets:
------------------------
total annotations: 44783
intents: 25
slots: 36
split: randomly
training utterances size: 31279
validation utterances size: 4462
test utterances size: 9042
median (mean) depth of the trees: 2 (2.54)
the median (mean) length of the utterances: 8 (8.93)
entropy: 5 (5.12)

My impressions on this data:
----------------------------

Upon analysing closely this dataset, the data looks natural to me, where intent and slots are very clearly marked.
Mostly work on task oriented intent and slot-filling work has been restricted to one intent per query and one slot label per token, and thus cannot model complex compositional requests. 
Sonal et al. (2018), have shown in their work that their hierarchical annotation scheme for semantic parsing allows the representation of compositional queries, and can be efficiently and accurately parsed by standard constituency parsing models. Their poposed hierarchical generalization of traditional intents and slots, allows the representation of complex nested queries, leading to 30% higher coverage of user requests and it also stated representation can be annotated with high agreement. These indicates a reliable usage of the data in the actual system.

Though the data well suited for tree-structured representation, but for arbitrary graphs, such as Abstract Meaning Representation (Banarescu et al., 2013) and Alexa Meaning Representation (Fan et al., 2017) this data is not very suitable. The limitation of this data as per my knowledge could be how these approaches (Banarescu et al., 2013; Fan et al., 2017) can represent complex constructions that this data may fail to do so.

References:
----------- 
1. Gupta, Sonal, et al. "Semantic parsing for task oriented dialog using hierarchical representations." arXiv preprint arXiv:1810.07942 (2018).
2. Laura Banarescu, Claire Bonial, Shu Cai, Madalina Georgescu, Kira Griffitt, Ulf Hermjakob, Kevin Knight, Philipp Koehn, Martha Palmer, and Nathan Schneider. 2013. Abstract meaning representation for sembanking. In Proceedings of the 7th Linguistic Annotation Workshop and Interoperability with Discourse, pages 178–186.
3. Xing Fan, Emilio Monti, Lambert Mathias, and Markus Dreyer. 2017. Transfer learning for neural semantic parsing. In Proceedings of the 2nd Workshop on Representation Learning for NLP, ACL.
