# Dataset Description: Holl-E
description by Michael Hanna
## Qualitative Description
### Overview
**[Holl-E](https://github.com/nikitacs16/Holl-E) is a text dataset** consisting of movie reviews, comments and plots, as well as artificial conversations about these movies, which are informed by the aforementioned materials.

As such, **it is most obviously useful for one type of dialogue system: chatbots that discuss movies**. Despite this, Holl-E's overarching goal is more ambitious; it aims to model conversation in a more humanlike way, by including background information that humans might know before entering a conversation. It contrasts itself with datasets and models that model conversation as a sequence of utterances lacking broader context. So, **Holl-E is actually aimed at open-domain dialogue systems**, even though the techniques it uses might seem related to multi-domain systems (e.g. Alexa) that occasionally answer questions about movies.

Because of this, the **models tested on Holl-E include** not only traditional **sequence-generation chatbots,** which respond solely based on the previous utterances in the conversation, but also **span prediction models,** which respond using passages in the background text, **and generate-or-copy models**, which choose between generation and copying words from the background text.

### Data collection and usage
**The Holl-E data consists of background information from IMDB reviews and Wikipedia movie plots, and conversations about these movies, written by Amazon Turkers**. The movies in the dataset were selected based on popularity metrics from IMDB; the most popular were chosen.

Each conversation consists of a dialogue between "Speaker 1" and "Speaker 2", fictitious speakers conversing about a movie. **One Turker wrote each conversation, playing the role of both speakers (no real two-person dialogue took place)**. Turkers created conversations by continuing dialogues given an existing opening statement.

To allow Speaker 2 to be modeled by span-prediction models, **Speaker 2 consists only of spans from the background passage**, with minimal material added to improve response fluency. In contrast, Speaker 1 was not subject to constraints, in order to make the data more natural. At training and test time, **models predict an utterance of Speaker 2 given the background information and three previous utterances** (two from Speaker 1, and one from Speaker 2). The purely generative models are given only the previous utterances.

### Practical information
**The data is available in .json format and in .bin format**; these are the formats used by the models tested on it. **It contains no special annotation**; each example includes a conversation, the name of the movie it is about, and the relevant background information. **No information is made available about licensing**.

## Quantitative Description
The data is split into train / dev / test sets; however, the stats reported below are for all 3 splits combined. They contain 80% / 10% / 10% of the data, respectively.
#### Length:
(\# chats): 9071

(\# utterances): 90810

(\# conversational turns): 82546

(\# background documents): 19714

#### Mean length ; std. dev:
chat (words): 153.07; 46.35

chat (utterances): 10.01; 1.14

utterance (words): 15.29; 11.12


#### Vocab size:
 9163 words (excluding punctuation)

#### Entropy (word): 
Speaker 1 (user): 8.69 bits

Speaker 2 (model): 9.48 bits

Both: 9.25 bits

## Analysis
So, how natural is this dataset? Ultimately, **I don't feel that this dataset is entirely natural**, mostly because of the constraint imposed on Speaker 2. While it is good that the speakers in the dataset reference real background knowledge, the degree to which real-life speakers do so is less than what is portrayed in this dataset. 

For example,

> I completely understand why this movie got such terrible reviews; after all, it marked the first "let\'s make a quick buck" entry in the two beloved Alien and Predator franchises and was done by a director who many felt didn\'t treat the material with the respect it deserved.

sounds like a podcast recorded by well-informed movie reviewers, rather than an off-the-cuff conversation. Normal speakers might reference background info, but only vaguely; such detailed knowledge of a movie seems uncommon.

Besides that small problem, **I also take issue with the evaluation method and conclusions drawn by the authors, given the constraints placed on Speaker 2**. The authors claim that the span-based model outperforms the generate / copy-or-generate models, but this seems to be trivially true. Of course, since the targets were drawn as spans from the background material, it was advantageous to use the span-based model; we're good at span-based QA, so **achieving good performance on this dataset using span prediction shouldn't be hard**. This is especially true because their metric, ROUGE, uses n-gram overlap, benefitting predictions that use the same words as the target.

That said, **I can see the approach taken in this dataset being used in real life**. Imagine a general-purpose chatbot conversing with a user. The user begins to talk about a specific movie (or other specific topic). The chatbot can then search for information about that topic, and then use span prediction or copy-or-generate models to generate an informed response. Although span-prediction would likely produce unrealistically intelligent responses, these responses would certainly still be serviceable. That said, such an approach would leave little room for novelty, or an dialogue agent that has opinions of its own; it could only parrot those of its source material.

In conclusion, I don't think that this dataset is particularly natural, and I don't think that model performance on it is an especially useful argument against copy-or-generate models. However, I do think that the paradigm promoted by this dataset has the potential to make general-purpose chatbots more capable of intelligent conversation.