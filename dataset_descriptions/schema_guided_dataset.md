#### Schema-Guided Dialogue Dataset

###### What kind of data is it?
The data consists of over 20k annotated multi-domain, task-oriented conversations between a human and a virtual assistant.
There are interactions with services and APIs from 20 domains, such as banking, calendar, or weather.
All of the data is written.

###### How was it collected?
The authors used a dialogue simulator to generate outlines and then paraphrased them to get natural utterances.

###### What kind of dialog system is it designed for?
It is designed for a task-oriented virtual assistant with a large variety of functions.

###### What kind of annotation is present?
The utterances are interpreted as frames.
The annotations include all slots that were relevant to the utterance and all of the mentioned values.
The annotations were obtained automatically, since they were a part of the data generation.
There are also files that fully list all possible frames, their slots and the possible values of the slots.

###### What format is it stored in?
The data is divided into a train, dev, and test datasets.
Each of them contains a number of JSON files.
The JSON files contain several conversations which are conveniently stored in an array of maps.

###### What license is it under?
The dataset is distributed under the Creative Commons Attribution Share Alike 4.0 International license.
It can be used for any purpose, modificated and distributed.
The authors offer no warranty and disclaim all liability for any damages.

###### Statistical analysis

|  Dataset |  # dialogues |  #turns |# sentences |  # words |  Avg turns/dialog |  Avg sentences/dialog |  Avg words/dialog |  Vocab size |  User entropy |  System entropy |
|----------|--------------|---------|----------|-------------------|-----------------------|-------------------|-------------|---------------|-----------------|------------|
|  train |  16142 |  329964 |  605557 |  3217528 |  20.44(6.89) |  37.51(13.41) |  199.33(80.59) |  35818 |  4.52 |  4.43 |
|  dev |  2482 |  48726 |  87573 |  470873 |  19.63(7.23) |  35.28(13.99) |  189.72(85.58) |  14813 |  4.16 |  4.05 |
|  test |  4201 |  84594 |  157161 |  880062 |  20.14(8.34) |  37.41(16.34) |  209.49(98.67) |  18878 |  4.31 |  4.21 |

The numbers in parentheses are standard deviations

###### Overall feeling
It is super easy to work with this dataset, because of the format.
I expected it to be more obvious that the data is automatically generated, but it even contains misspellings and grammatical errors.
Some of the utterances are pretty complex, which is definitely a good thing.
It even contains some examples of the system misunderstanding the situation.
One problem might be that this dataset is really complex.
There are so many possible domains and intents, so it seems like it would take a lot of time to fully work with all of the dataset's features.

