# NPFL099 - Assignment 1&2

# Dataset - Wizard of Wikipedia

## Summary of the data
- *What kind of data is it? (domain, modality)*

The data were collected with an intent to build and train language models for open-domain dialogues. As such, the
content of conversations was about one domain/topic.

- *How was it collected?*

There were always two participants in each dialogue session - an apprentice and a wizard. An apprentice played the role
of a curious agent desired to get more information about some topics. A wizard, on the other hand, played the role
of an expert who was shown relevant information (paragraphs from Wikipedia articles). Eventually, the conversation
repeated until one of the agents ended it. The number of turns was at least 4 or 5 for each participant, and the one, who
ended the dialogue, was a priori randomly chosen.

The author was designed GUI for conducting these conversations.

- *What kind of dialogue system or dialogue system component is it designed for?*

As answered in the first question, this data set was collected for developing open-domain dialogue systems.

- *What kind of annotation is present (if any at all), how was it obtained (human/automatic)?*

There are annotations for three different events - topic ($x_1$) and the last two turns ($x_t, x_{t_1}$).
On each occasion, a **human annotator** was provided with top 7 articles (first paragraph only), and he or she chose
the best one via the GUI.

- *What format is it stored in?*

The data are stored in JSON file. Within this file, there is a list where each item represents a single dialogue.
Each dialogue item is represented by a dictionary with 5 keys: *chosen topic, persona, wizard_eval, dialog, chosen_topic_passage*.
*Chosen topic* is a string indicating a topic of a given dialogue. *Persona* is a short characteristic of a person
in order to choose a discussion topic naturally. These persons were sourced form Persona-Chat dataset. *Wizard_eval*
is an integer on scale from 1 to 5 (with 5 being the best) indicating how much a human evaluator liked the conversation.
*Chosen_topic_passage* is a list of 7 paragraphs for the stage $x_1$ defined in the previous answer. Eventually
*Dialog* contains individual turns of the dialog, an order of speakers and so on.


- *What is the license?*

This dataset, **Wizard of Wikipedia**, is a part of ParlAI package having MIT License.

## Basic statistics
In total, there are 22,311 dialogues consisting of 201,999 turns. The dataset consists of 309,339 sentences or
3,359,456 words.

Further, we can focus on a distribution of individual dialogs. A single dialog has 
9.05 turns in average with standard deviation of 1.04. The quartile distrubtion is then as follow

|   | **Q1** | **Q2** | **Q3** |
|:-:|--------|--------|-------:|
| **turns** | 8 | 10 | 10 |

From these measures, it seems that involved participants were given quite rigid instructions of how a dialogue
should look like as a length of dialogues does not any significantly differ.

In terms of distribution of sentences and words in individual dialogues, please, see the table below:

|   | **mean** | **std** | **Q1** | **Q2** | **Q3** |
|:-:|----------|---------|--------|--------|-------:|
| **sentences** | 13.86 | 3.91 | 11 | 13 | 16 |
| **words** | 148.15 | 45.04 | 116 | 143 | 174 |

The vocabulary of the wizard is broader than the one of the apprentice. This fact, in my opinion, might have been anticipated
considering the fact that the wizard had an access to external knowledge. The vocabulary of the wizard consists of 40,271 words while the apprentice
has 28,503 words in his dictionary. The union of these two vocabularies contains 47,820 unique words. (I did only a brief text-preprocessing
when I made all the text to be typed in lower case.)

Top 10 most frequent words in wizard and apprentice vocabularies are:

| N | Wizard | Apprentice |
| - | ------ | ---------- |
| 1 | . | . |
| 2 | the | i |
| 3 | i | ? |
| 4 | a | the |
| 5 | and | a |
| 6 | of | that |
| 7 | it | to |
| 8 | is | it |
| 9 | to | it |
| 10 | in | you |

Finally, we also need to calculate entropy. For this purpose, I relied on the formula from lecture slides.
Then, *wizard entropy* is **7.324** and *apprentice entropy* is **6.884**.